Here is a repo of my Google Colab Projects and Python/Machine Learning training I have undertaken.

I mainly just use my Google Drive to keep my notebooks up to date, so I don't push to GitLab as frequently as I work on those Projects, but I will try to keep this repo as up to date as possible. 


PLEASE NOTE: If you are a potential employer here to check out my repo please be aware that this a lot of this is a work in progress so if you try running some of the code some of it might bring up errors. Most of it should be ship shape however. I recommend looking at my notebooks in the 'Stock Market' and 'Codecadamy' folders. :)

GOOGLE COLAB LINKS TO RUN THE CODE:

Portfolio Optimisation for Final Codecademy Project:
https://colab.research.google.com/drive/1GH8z0bkOaW5H_UuoZwuN0MxViwdBLr7b?usp=sharing

Buffettology:
https://colab.research.google.com/drive/1zJtJrJNKT25RUcDXfMLV-ouvj1QmPRdR?usp=sharing
